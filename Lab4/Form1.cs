﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void autoRunPrograms()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            string[] run1 = new string[50];
            string[] run2 = new string[50];

            try
            {
                run1 = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run").GetValueNames();
            }
            catch (Exception e)
            {
                listBox1.Items.Add(e);
            }

            try
            {
                run2 = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run").GetValueNames();
            }
            catch (Exception e)
            {
                listBox1.Items.Add(e);
            }

            for (int i = 0; i < run1.Length; i++)
            {
                try
                {
                    listBox1.Items.Add(run1[i]);
                }
                catch (Exception e)
                {
                    listBox1.Items.Add(e);
                }
            }

            for (int i = 0; i < run2.Length; i++)
            {
                try
                {
                    listBox2.Items.Add(run2[i]);
                }
                catch (Exception e)
                {
                    listBox2.Items.Add(e);
                }
            }
        }

        public void outPutSchedule()
        {
            listBox3.Items.Clear();
            string[] item1 = new string[50];
            try
            {
                item1 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tree").GetSubKeyNames();
            }
            catch (Exception e)
            {
                listBox3.Items.Add(e);
            }

            for (int i = 0; i < item1.Length; i++)
            {
                try
                {
                    listBox3.Items.Add(item1[i]);
                }
                catch (Exception e)
                {
                    listBox3.Items.Add(e);
                }
            }
        }

        private void addNewAutoRunProgram()
        {
            var key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            var link = textBox1.Text;
            key.SetValue(link, link);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            autoRunPrograms();
            outPutSchedule();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            addNewAutoRunProgram();
        }

        public void ExportKey(string RegKey, string SavePath)
        {
            string path = "\"" + SavePath + "\"";
            string key = "\"" + RegKey + "\"";
            Process proc = new Process();
            try
            {
                proc.StartInfo.FileName = "regedit.exe";
                proc.StartInfo.UseShellExecute = false;
                proc = Process.Start("regedit.exe", "/e " + path + " " + key + "");
                if (proc != null) proc.WaitForExit();
            }
            finally
            {
                if (proc != null) proc.Dispose();
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            ExportKey(comboBox1.Text, Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\File.reg");
        }


        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}